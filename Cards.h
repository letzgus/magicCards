#pragma once

#include <string>


//Aufgabe Nr.2

struct Cards {
    //cmc and count are also strings to make it easier for the getListOfCards function
public:
    std::string name;
    std::string mana;
    std::string cmc;
    std::string type;
    std::string count;
};