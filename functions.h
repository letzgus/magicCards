#pragma once

#include <string>
#include <iostream>
#include <list>
#include "Cards.h"


std::list<Cards> getListOfCards(const std::string &filename);

void saveCardsToFile(const std::list<Cards> &CardsList, const std::string &filename);

std::list<std::string> referenceNames(const std::string &referenceFile);

void levenshteinDistance(const std::list<std::string> &intactNames, std::list<Cards> &brokenNames);




