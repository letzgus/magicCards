#include "levenD.h"
#include "Cards.h"
#include "functions.h"

int main() {

    std::list<Cards> scrambledCards;
    std::list<std::string> referenceCardNames;

    /* on VsCode use this instead
     //Take scrambled cards and put them in a list
    scrambledCards = getListOfCards("../../scrambled.txt");
    //take reference names and put them in a list
    referenceCardNames = referenceNames("../../reference.txt");
     */

    //Take scrambled cards and put them in a list
    scrambledCards = getListOfCards("../scrambled.txt");
    //take reference names and put them in a list
    referenceCardNames = referenceNames("../reference.txt");

    //calculate levenshteinDistance and create new file with intact names (if existing)
    levenshteinDistance(referenceCardNames, scrambledCards);



    return 0;
}
